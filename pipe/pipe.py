import os
import json
import glob
from pathlib import Path

import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_logger


MAX_FILES_UPLOAD_LIMIT = 10

BITBUCKET_BASE_URL = "https://bitbucket.org"
BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"


logger = get_logger()

schema = {
    'BITBUCKET_USERNAME': {'type': 'string', 'required': True},
    'BITBUCKET_APP_PASSWORD': {'type': 'string', 'required': True},
    'FILENAME': {'type': 'string', 'required': True},
    'ACCOUNT': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_OWNER')},
    'REPOSITORY': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_REPO_SLUG')},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class UploadFilePipe(Pipe):

    def run(self):
        super().run()

        logger.info('Executing the pipe...')
        username = self.get_variable('BITBUCKET_USERNAME')
        app_password = self.get_variable('BITBUCKET_APP_PASSWORD')
        files = self.get_variable('FILENAME')

        account = self.get_variable('ACCOUNT')
        repository = self.get_variable('REPOSITORY')

        url = f"{BITBUCKET_API_BASE_URL}/repositories/{account}/{repository}/downloads"
        auth = HTTPBasicAuth(username, app_password)

        # https://meta.stackoverflow.com/questions/368287/canonical-for-why-are-wildcards-not-being-expanded-by-subprocess-call-run-popen
        # wildcard preprocessing before run subprocess
        if "*" in files:
            files_list = []
            for file in files.split(' '):
                files_list.extend(glob.glob(file, recursive=True))
        else:
            files_list = files.split(' ')

        # only uniq values
        files_list = list(set(files_list))

        if not files_list:
            self.fail(f"Files by pattern {files} not found.")

        if len(files_list) > MAX_FILES_UPLOAD_LIMIT:
            self.fail(f'Found {len(files_list)} files to upload. Supported max '
                      f'number of files to upload {MAX_FILES_UPLOAD_LIMIT}.')

        multiple_files = []
        for filename in files_list:
            if not Path(filename).is_file():
                self.fail(f"File {filename} doesn't exist.")
            multiple_files.append(('files', (filename, FileOnDemand(filename, 'rb'))))

        file_names = [fname[1][0] for fname in multiple_files]
        logger.info(f"Start uploading {len(file_names)} files"
                    f" {file_names}...")
        response = requests.post(url, auth=auth, files=multiple_files)

        # Bitbucket API returns status code 201 'Created' after success upload
        # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/downloads#post
        if response.status_code == 201:
            link = f"{BITBUCKET_BASE_URL}/{account}/{repository}/downloads"
            for filename in file_names:
                basename = os.path.basename(filename)
                logger.info(f"You can download the file by clicking in this link: {link}/{basename}")
            self.success(f"Successfully uploaded files {file_names} to {link}")
        elif response.status_code == 401:
            # clarifying message for users
            self.fail(
                f"API request failed with status 401. Check your username and app password and try again.")
        else:
            try:
                data = response.json()
            except json.decoder.JSONDecodeError:
                data = None

            message = data['error']['message'] if data and data.get('error') else response.text

            self.fail(
                f"Failed to upload files {file_names}. "
                f"Status code: {response.status_code}. "
                f"Message: {message}."
            )


class FileOnDemand:

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def read(self):
        with open(self.filename, self.mode) as f:
            return f.read()


if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = UploadFilePipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
